IMAGE_NAME=registry.shinecams.com:5000/images/ubuntu
IMAGE_TAG=16.04
IMAGE_FULL_NAME=$(IMAGE_NAME):$(IMAGE_TAG)
#=========================================================

build-image:
    docker build -t $(IMAGE_FULL_NAME) .
    docker tag $(IMAGE_FULL_NAME) $(IMAGE_NAME):latest

push:
    docker push $(IMAGE_FULL_NAME)
    docker push $(IMAGE_NAME):latest
